<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="style.css">
</head>

<?php

error_reporting(E_ERROR | E_PARSE);

class Student
{
    public $name;
    public $gender;
    public $faculty;
    public $dob;
    public $address;
    public $picture;
}

class StudentFilter
{
    public $faculty;
    public $keyword;
}

function console_log($data)
{
    $output = json_encode($data);

    echo "<script>console.log('{$output}' );</script>";
}

function handle_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);

    return $data;
}

$faculties = array(
    "MAT" => "Khoa học máy tính",
    "KDL" => "Khoa học vật liệu"
);

$student1 = new Student();
$student1->name = 'Nguyễn Văn A';
$student1->faculty = 'MAT';

$student2 = new Student();
$student2->name = 'Trần Thị B';
$student2->faculty = 'MAT';

$student3 = new Student();
$student3->name = 'Nguyễn Hoàng C';
$student3->faculty = 'KDL';

$student4 = new Student();
$student4->name = 'Đinh Quang D';
$student4->faculty = 'KDL';

$students = array($student1, $student2, $student3, $student4);

$filter = new StudentFilter;

function init_data()
{
    global $filter;

    session_start();
    if (isset($_SESSION['student-filter'])) {
        $filter = $_SESSION['student-filter'];
    }
}

init_data();

if (isset($_POST['submit-search'])) {
    foreach ($filter as $key => $value) {
        $filter->$key = handle_input($_POST[$key]);
    }

    session_start();
    $_SESSION['student-filter'] = $filter;
}

?>

<body>
    <div class="body">
        <div>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                <div class="flex-start">
                    <div class="filter-label">
                        Khoa
                    </div>
                    <select id="filter-faculty" class="filter-input" name="faculty">
                        <option selected value=""></option>
                        <?php
                        foreach ($faculties as $key => $value) {

                            if ($key == $filter->faculty) {
                                echo "<option selected value='$key'>$value</option>";
                            } else {
                                echo "<option value='$key'>$value</option>";
                            }
                        }
                        ?>
                    </select>
                </div>

                <div class="flex-start">
                    <div class="filter-label">
                        Từ khóa
                    </div>

                    <input class="filter-input" id="filter-keyword" name="keyword" type='text' value="<?php echo isset($filter->keyword) ? $filter->keyword : ''; ?>" />

                </div>

                <div class="mt-25 flex-space-around">
                    <button type="button" class="btn" onclick="clearFilterInput()">
                        Xóa
                    </button>
                    <input class="btn" type='submit' name="submit-search" value="Tìm kiếm">
                </div>
            </form>
        </div>
        <div class="w-65 mt-25 flex-space-between">
            <?php
            echo "Số sinh viên tìm thấy: " . count($students);
            ?>
            <div>
                <a class="btn mr-20" href="register.php"> Thêm </a>
            </div>
        </div>
        <div class="w-75 mt-25">
            <table class="w-100">
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th>Action</th>
                </tr>

                <?php
                for ($i = 0; $i < count($students); $i++) {
                    $student = $students[$i];
                    $index = $i + 1;
                    $faculty = $faculties[$student->faculty];

                    echo "<tr>
                            <td>$index</td>
                            <td>$student->name</td>
                            <td>$faculty</td>
                            <td>
                                <input class='table-btn' type='button' value='Xóa'>
                                <input class='table-btn' type='button' value='Sửa'>
                            </td>
                        </tr>";
                }
                ?>
            </table>
        </div>
    </div>

    <script type="text/javascript">
        function resetSelectElement(selectElement) {
            var options = selectElement.options;

            // Look for a default selected option
            for (var i = 0, iLen = options.length; i < iLen; i++) {

                if (options[i].defaultSelected) {
                    selectElement.selectedIndex = i;
                    return;
                }
            }

            // If no option is the default, select first or none as appropriate
            selectElement.selectedIndex = 0; // or -1 for no option selected
        }

        function clearFilterInput() {
            document.getElementById('filter-keyword').value = '';
            resetSelectElement(document.getElementById('filter-faculty'));
        }
    </script>

</body>

</html>